<?php

namespace App\Controller;

use Goutte\Client;
use Browser\Casper;
use App\Form\ScrapType;
use Sunra\PhpSimple\HtmlDomParser;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ScrapController extends AbstractController
{
    /**
     * @Route("/", name="scrap")
     */
    public function index(Request $request)
    {
        $data = [];
        $content = false;
        $form = $this->createForm(ScrapType::class, $data);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            dump($form['url']->getData());
            $url = $form['url']->getData();
            $urlExplode = explode("/", $url);
            $code = array_reverse(explode('-', $urlExplode[4]));
            dump($code[0]);

            $client = \Symfony\Component\HttpClient\HttpClient::create(['headers' => [
                'User-Agent' => 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_3 like Mac OS X) AppleWebKit/602.1.50 (KHTML, like Gecko) CriOS/56.0.2924.75 Mobile/14E5239e Safari/602.1',
            ]]);
            $response = $client->request("GET", "https://www.centrale-canine.fr/api/dog/". $code[0] ."?_format=json");
            $content = $response->toArray();
            dump($content);
            
        }

        return $this->render('scrap/index.html.twig', [
            'controller_name' => 'ScrapController',
            'form' => $form->createView(),
            'dog' => $content,
        ]);
    }
    
    // TODO: Faire un scrap de l'ensemble de l'api
}
